//
//  ViewController.swift
//  Example
//
//  Created by Alfredo Luco on 04-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import UIKit
//First import Eureka and DSYCustomEureka Row
import DSYCustomEurekaRow
import Eureka

class ViewController: FormViewController, DSYTextPresenterCellDelegate { //Change UIViewController to FormViewController

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //change the separation style to none
        self.tableView.separatorStyle = .none
        form +++ Section()
            //simply add presenter row
            <<< DSYTextPresenterRow(){
                $0.cellHeaderText = "Algo"
                $0.cellSeparatorViewColor = UIColor.green
                $0.tag = "Nombre"
                $0.cell.delegate = self
                $0.sendButtonTintColor = UIColor.brown
                $0.sendButtonText = "Send"
                $0.sendButtonColor = UIColor.cyan
        }
        
            <<< DSYTextPresenterRow(){
                $0.cellHeaderText = "Otro Campo"
                $0.cellHeaderTextColor = UIColor.blue
                $0.cellFieldText = "Segunda Fila"
                $0.cellFieldTextColor = UIColor.brown
        }
            <<< DSYEmailPresenterRow(){
                $0.cellHeaderText = "Otro Campo"
                $0.cellHeaderTextColor = UIColor.blue
                $0.cellFieldText = "Segunda Fila"
                $0.cellFieldTextColor = UIColor.brown
        }
            <<< DSYDatePresenterRow(){
                $0.cellHeaderText = "Fecha"
                $0.cellFieldText = "algo"
        }
            <<< DSYPickerPresenterRow(){
                $0.data = ["male", "female"]
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func didUpdateField(field: String?, value: String?) {
        print("field: \(field as Any) value: \(value as Any)")
    }

}

