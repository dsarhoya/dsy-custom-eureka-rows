Pod::Spec.new do |s|
  # 1
  s.platform = :ios
  s.ios.deployment_target = '8.0'
  s.name         = "DSYCustomEurekaRow"
  s.summary = "DSYCustomEurekaRows is a framework that generates a presenter row from your custom cell and your custom view controller"
  s.requires_arc = true

  # 2
  s.version      = "0.0.8"

  # 3
  # s.licence = { :type => "MIT", :file => "LICENSE" }

  # 4
  s.author = { "dsarhoya " => "contacto@dsy.cl" }

  # 5
  s.homepage = "https://bitbucket.org/dsarhoya/dsy-custom-eureka-rows"

  # 6
  s.source = { :git => "https://bitbucket.org/dsarhoya/dsy-custom-eureka-rows", :tag => "0.0.8" }

  # 7
  s.ios.frameworks = 'UIKit', 'Foundation'
  s.dependency 'Eureka', '~> 4.0'

  # 8
  s.ios.source_files = 'Sources/**/*.swift'
  
  #9
  #s.ios.resource_bundle = {"DSYCustomEurekaRow" => 'Sources/Media.xcassets'}
  s.resources = "Sources/**/*.xcassets"

end
