//
//  DSYPickerViewController.swift
//  DSYCustomEurekaRow
//
//  Created by Alfredo Luco on 07-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import UIKit
import Eureka

public class DSYPickerViewController: FormViewController, TypedRowControllerType,DSYCellProtocol {

    public typealias RowValue = String
    public var row: RowOf<String>!
    public var onDismissCallback: ((UIViewController) -> Void)?
    
    public var cell: DSYTextPresenterRowDelegate? = nil
    public var sendButton: DSYSendButtonRowDelegate? = nil
    public var picker: DSYPickerRowDelegate? = nil
    
    public override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorStyle = .none
        
        form +++ DSYPickerRow(){
            
            if let cell = self.cell{
                $0.cellFieldText = cell.textFieldPlaceholder
                $0.cellFieldTextColor = cell.cellFieldTextColor
                $0.cellSeparatorViewColor = cell.cellSeparatorViewColor
                $0.cellFieldTextFont = cell.cellFieldTextFont
                $0.cellHeaderText = cell.cellHeaderText
                $0.cellHeaderTextFont = cell.cellHeaderTextFont
                $0.cellHeaderTextColor = cell.cellHeaderTextColor
                $0.icon = cell.icon
                $0.cellBackgroundColor = cell.cellBackgroundColor
            }
            
            if let picker = self.picker{
                $0.data = picker.data
            }
            
            if let button = self.sendButton{
                $0.cellButtonColor = button.sendButtonColor
                $0.cellButtonText = button.sendButtonText
                $0.cellButtonTintColor = button.sendButtonTintColor
                $0.cellButtonFont = button.sendButtonFont
            }
            
            $0.cell.delegate = self
            
            var ruleSet = RuleSet<String>()
            ruleSet.add(rule: RuleRequired(msg: "Debe rellenar el texto"))
            ruleSet.add(rule: RuleMinLength(minLength: 1, msg: "Debe rellenar el texto"))
            $0.add(ruleSet: ruleSet)
            
            }.cellUpdate({cell,row in
                if(row.isValid){
                    self.row.value = row.value
                }
            })
    }

    public override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    public func didTouchButton() {
        self.update()
    }
    
    func update(){
        if(self.form.validate().count <= 0){
            self.onDismissCallback!(self)
        }else{
            let alertController = UIAlertController(title: "Error", message: self.form.validate().first!.msg, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }

}
