//
//  DSYDatePicker.swift
//  DSYCustomEurekaRow
//
//  Created by Alfredo Luco on 07-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import UIKit

class DSYDatePicker: UIDatePicker {
    
    //MARK: - Properties
    var toolbar: UIToolbar? = nil
    weak var textField: UITextField? = nil
    private lazy var dateFormatter: DateFormatter = {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "dd-MM-yyyy"
        return dateformatter
    }()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.maximumDate = Date()
    }
    
    func setup(){
        //toolbar
        self.toolbar = UIToolbar()
        self.toolbar!.barStyle = .default
        self.toolbar!.isTranslucent = true
        self.toolbar!.tintColor = UIColor.black
        self.toolbar!.sizeToFit()
        
        //done button
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
        doneButton.accessibilityLabel = "done_picker"
        
        //pair toolbar with button
        self.toolbar!.setItems([doneButton], animated: false)
        self.toolbar!.isUserInteractionEnabled = true
        
        self.textField?.inputView = self
        self.textField?.inputAccessoryView = self.toolbar
        
        self.addTarget(self, action: #selector(dateChanged), for: .valueChanged)
        self.datePickerMode = .date
        
    }
    
    @objc func dateChanged(){
        self.textField?.text = self.dateFormatter.string(from: self.date)
    }
    
    @objc func donePicker(){
        self.textField?.text = self.dateFormatter.string(from: self.date)
        self.textField?.resignFirstResponder()
    }
    
}

