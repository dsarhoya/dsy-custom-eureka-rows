//
//  DSYButtonCell.swift
//  DSYCustomEurekaRow
//
//  Created by Alfredo Luco on 06-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import UIKit
import Eureka

//Button Cell
open class DSYButtonCell: Cell<Bool>,CellType {

    //button
    @IBOutlet weak var button: UIButton!
    
    //init
    required public init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        let button = UIButton()
        self.button = button
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.button.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.addSubview(self.button)
        self.makeConstraints()
    }
    
    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //CellType delegate
    
    //setup
    override open func setup() {
        super.setup()
        selectionStyle = .none
        self.button.layer.cornerRadius = 4.0
        self.button.isEnabled = false
    }
    
    //Update
    override open func update() {
        super.update()
        self.button.backgroundColor = (row as? DSYButtonRowDelegate)?.cellButtonColor ?? UIColor.blue
        self.button.setTitleColor((row as? DSYButtonRowDelegate)?.cellButtonTintColor ?? UIColor.white, for: .normal)
        self.button.setTitle((row as? DSYButtonRowDelegate)?.cellButtonText ?? "Enviar", for: .normal)
        self.button.titleLabel?.font = (row as? DSYButtonRowDelegate)?.cellButtonFont ?? UIFont.systemFont(ofSize: 17.0)
    }
    
    //Constraints Implementation
    func makeConstraints(){
        let views : [String : Any] = [
            "button" : self.button
        ]
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(40)-[button(40)]-|", options: [], metrics: nil, views: views))
        self.contentView.addConstraint(NSLayoutConstraint(item: self.button, attribute: .centerX, relatedBy: .equal, toItem: self.contentView, attribute: .centerX, multiplier: 1.0, constant: 1.0))
        self.contentView.addConstraint(NSLayoutConstraint(item: self.button, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 200))
    }
    
}
