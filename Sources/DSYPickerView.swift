//
//  DSYPickerView.swift
//  DSYCustomEurekaRow
//
//  Created by Alfredo Luco on 07-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import Foundation
class DSYPickerView: UIPickerView,UIPickerViewDelegate, UIPickerViewDataSource {
    
    //MARK: - Properties
    var toolbar: UIToolbar? = nil
    weak var textField: UITextField? = nil
    var data: [String] = []
    private var lastIndex: Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setup(){
        self.translatesAutoresizingMaskIntoConstraints = false
        //toolbar
        self.toolbar = UIToolbar()
        self.toolbar!.barStyle = .default
        self.toolbar!.isTranslucent = true
        self.toolbar!.tintColor = UIColor.black
        self.toolbar!.sizeToFit()
        
        //done button
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(donePicker))
        doneButton.accessibilityLabel = "done_picker"
        
        //pair toolbar with button
        self.toolbar!.setItems([doneButton], animated: false)
        self.toolbar!.isUserInteractionEnabled = true
        self.toolbar!.layoutIfNeeded()
        
        self.textField?.inputView = self
        self.textField?.inputAccessoryView = self.toolbar
        
        dataSource = self
        delegate = self
        self.lastIndex = self.getLastIndex()
        self.selectRow(self.lastIndex, inComponent: 0, animated: false)
    }
    
    @objc func donePicker(){
        if(self.lastIndex < self.data.count){
            self.textField?.text = self.data[self.lastIndex]
        }
        self.textField?.resignFirstResponder()
    }
    
    //MARK: - Picker View Data Source
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.data.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if(row < 0 || row >= self.data.count){
            return nil
        }
        return self.data[row]
    }
    
    //MARK: - Picker View Delegate
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        guard row < self.data.count else{ return }
        
        self.textField?.text = self.data[row]
        self.lastIndex = row
    }
    
    //MARK: - Other Methods
    func getLastIndex()->Int{
        
        if(self.textField != nil){
            for (index, element) in self.data.enumerated(){
                if(element == self.textField!.text){
                    return index
                }
            }
        }
        return 0
    }
    
}
