//
//  DSYCustomPresenterRow.swift
//  DSYCustomEurekaRows
//
//  Created by Alfredo Luco on 04-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import Foundation
import Eureka
//
//public final class DSYCustomPresenterRow<cellType: CellType,VCType: TypedRowControllerType>: SelectorRow<cellType,VCType> where cellType : BaseCell, VCType: UIViewController, VCType.RowValue == cellType.Value {
//    
//    required public init(tag: String?) {
//        super.init(tag: tag)
//    }
//    
//    required public init(tag: String?,cellNIB: String, vcName: String) {
//        super.init(tag: tag)
//        
//        cellProvider = CellProvider<cellType>(nibName: cellNIB)
//        
//        presentationMode = .show(controllerProvider: ControllerProvider.storyBoard(storyboardId: vcName, storyboardName: "Main", bundle: Bundle.main), onDismiss: { vc in
//                _ = vc.navigationController?.popViewController(animated: true)
//        })
//        
//    }
//    
//    
//}
