//
//  DSYTextViewController.swift
//  DSYCustomEurekaRow
//
//  Created by Alfredo Luco on 04-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import UIKit
import Eureka

public class DSYTextViewController:  FormViewController, TypedRowControllerType,DSYCellProtocol{

    //Typed Row Controller Type Delegate
    public typealias RowValue = String
    public var row: RowOf<String>!
    public var onDismissCallback: ((UIViewController) -> Void)?
    
    //Global Variables
    public var cell: DSYTextPresenterRowDelegate? = nil
    public var sendButton: DSYSendButtonRowDelegate? = nil
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorStyle = .none
        
        form +++ DSYTextRow(){
            
            if let cell = self.cell{
                $0.cellFieldText = cell.textFieldPlaceholder
                $0.textFieldPlaceholder = cell.textFieldPlaceholder
                $0.cellFieldTextColor = cell.cellFieldTextColor
                $0.cellSeparatorViewColor = cell.cellSeparatorViewColor
                $0.cellFieldTextFont = cell.cellFieldTextFont
                $0.cellHeaderText = cell.cellHeaderText
                $0.cellHeaderTextFont = cell.cellHeaderTextFont
                $0.cellHeaderTextColor = cell.cellHeaderTextColor
                $0.icon = cell.icon
                $0.cellBackgroundColor = cell.cellBackgroundColor
            }
            
            if let button = self.sendButton{
                $0.cellButtonColor = button.sendButtonColor
                $0.cellButtonText = button.sendButtonText
                $0.cellButtonTintColor = button.sendButtonTintColor
                $0.cellButtonFont = button.sendButtonFont
            }
            
            $0.cell.delegate = self
            
            $0.add(rule: RuleRequired(msg: "Debe rellenar el texto"))
            $0.add(rule: RuleMinLength(minLength: 1, msg: "Debe rellenar el texto"))
            
            }.cellUpdate({cell,row in
                if(row.isValid){
                    self.row.value = row.value
                }
            })
                
    }

    override public func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func didUpdate(field: String) {
        cell?.textFieldPlaceholder = field
        cell?.cellFieldText = field
    }
    
    public func didTouchButton() {
        self.update()
    }
    
    func update(){
        if(self.form.validate().count <= 0){
            self.onDismissCallback!(self)
        }else{
            let alertController = UIAlertController(title: "Error", message: self.form.validate().first!.msg, preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
}
