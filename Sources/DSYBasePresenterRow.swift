//
//  File.swift
//  MCEureka
//
//  Created by Matías Castro on 4/17/18.
//  Copyright © 2018 Matías Castro. All rights reserved.
//

import Foundation
import Eureka

open class DSYBasePresenterRow<T: CellType, VCType: TypedRowControllerType>: OptionsRow<T>, PresenterRowType where T: BaseCell, VCType: UIViewController, VCType.RowValue == T.Value {
    
    public var presentationMode: PresentationMode<VCType>?
    public var onPresentCallback: ((FormViewController, VCType) -> Void)?
    public typealias PresentedControllerType = VCType
    
    required public init(tag: String?) {
        super.init(tag: tag)
    }
    open override func customDidSelect() {
        super.customDidSelect()
        guard let presentationMode = presentationMode, !isDisabled else { return }
        if let controller = presentationMode.makeController() {
            controller.row = self
            controller.title = selectorTitle ?? controller.title
            onPresentCallback?(cell.formViewController()!, controller)
            presentationMode.present(controller, row: self, presentingController: self.cell.formViewController()!)
        } else {
            presentationMode.present(nil, row: self, presentingController: self.cell.formViewController()!)
        }
    }
}
