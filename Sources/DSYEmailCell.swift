//
//  DSYEmailCell.swift
//  DSYCustomEurekaRow
//
//  Created by Alfredo Luco on 06-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import UIKit
import Eureka

//Email Cell
public class DSYEmailCell: DSYCell {

    //Setup with keyboard type Email
    public override func setup() {
        super.setup()
        self.textField.keyboardType = .emailAddress
        row.displayValueFor = nil
        selectionStyle = .none
        self.textField.text = (row as? DSYTextPresenterRowDelegate)?.cellFieldText ?? ""
        row.displayValueFor = nil
    }
}
