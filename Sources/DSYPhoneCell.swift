//
//  DSYPhoneCell.swift
//  DSYCustomEurekaRow
//
//  Created by Alfredo Luco on 07-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import UIKit

//Phone Cell
public class DSYPhoneCell: DSYCell {

    public override func setup() {
        super.setup()
        //Keyboard like phone pad
        self.textField.keyboardType = .phonePad
        selectionStyle = .none
        self.textField.text = (row as? DSYTextPresenterRowDelegate)?.cellFieldText ?? ""
        row.displayValueFor = nil
    }
}
