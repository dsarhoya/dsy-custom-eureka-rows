//
//  DSYCell.swift
//  DSYCustomEurekaRow
//
//  Created by Alfredo Luco on 07-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import UIKit
import Eureka

public protocol DSYCellProtocol: class {
    func didTouchButton()
}

//DSY Cell is a generic cell for presented rows
public class DSYCell: Cell<String>,CellType, UITextFieldDelegate {
    
    //header field that can be use as title
    @IBOutlet weak var headerLabel: UILabel!
    
    //text field that canbe customize
    @IBOutlet weak var textField: UITextField!
    
    //icon
    @IBOutlet weak var icon: UIImageView!
    
    //separator view
    @IBOutlet weak var separatorView: UIView!
    
    //button
    @IBOutlet weak var button: UIButton!
    
    public weak var delegate: DSYCellProtocol? = nil
    
    //init
    required public init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        
        let headerLabel = UILabel()
        let textField = UITextField()
        let icon = UIImageView()
        let separatorView = UIView()
        let button = UIButton()
        
        self.headerLabel = headerLabel
        self.textField = textField
        self.icon = icon
        self.icon.contentMode = .scaleAspectFit
        self.separatorView = separatorView
        self.button = button
        
        self.headerLabel.translatesAutoresizingMaskIntoConstraints = false
        self.textField.translatesAutoresizingMaskIntoConstraints = false
        self.icon.translatesAutoresizingMaskIntoConstraints = false
        self.separatorView.translatesAutoresizingMaskIntoConstraints = false
        self.button.translatesAutoresizingMaskIntoConstraints = false
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        self.contentView.addSubview(self.headerLabel)
        self.contentView.addSubview(self.textField)
        self.contentView.addSubview(self.icon)
        self.contentView.addSubview(self.separatorView)
        self.contentView.addSubview(self.button)
        
        self.textField.delegate = self
        self.textField.becomeFirstResponder()
        self.button.isUserInteractionEnabled = true
        self.button.addTarget(self, action: #selector(changeRowValue), for: .touchUpInside)
        self.implementConstraints()
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // constraints implementation
    open func implementConstraints(){
        
        let views : [String : Any] = [
            "header" : self.headerLabel,
            "field" : self.textField,
            "icon" : self.icon,
            "view" : self.separatorView,
            "button" : self.button
        ]
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-(16)-[icon(30)]-(8)-[header]-(16)-|", options: [], metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-(16)-[icon(30)]-(8)-[field]-(16)-|", options: [], metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-(16)-[view]-(16)-|", options: [], metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[icon(30)]-(>=8)-[view(1)]-(40)-[button(40)]-|", options: [], metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[header]-(8)-[field]-(16)-[view(1)]-(40)-[button(40)]-|", options: [], metrics: nil, views: views))
        self.contentView.addConstraint(NSLayoutConstraint(item: self.button, attribute: .centerX, relatedBy: .equal, toItem: self.contentView, attribute: .centerX, multiplier: 1.0, constant: 1.0))
        self.contentView.addConstraint(NSLayoutConstraint(item: self.button, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 200))
        
    }
    
    //setup
    open override func setup() {
        super.setup()
        selectionStyle = .none
        self.textField.text = (row as? DSYTextPresenterRowDelegate)?.textFieldPlaceholder ?? ""
        row.displayValueFor = nil
    }
    
    //update
    open override func update() {
        super.update()
        self.button.backgroundColor = (row as? DSYButtonRowDelegate)?.cellButtonColor ?? UIColor.blue
        self.button.setTitleColor((row as? DSYButtonRowDelegate)?.cellButtonTintColor ?? UIColor.white, for: .normal)
        self.button.setTitle((row as? DSYButtonRowDelegate)?.cellButtonText ?? "Enviar", for: .normal)
        self.button.titleLabel?.font = (row as? DSYButtonRowDelegate)?.cellButtonFont ?? UIFont.systemFont(ofSize: 17.0)
        
        self.headerLabel.textColor = (row as? DSYTextPresenterRowDelegate)?.cellHeaderTextColor ?? UIColor.black
        self.headerLabel.font = (row as? DSYTextPresenterRowDelegate)?.cellHeaderTextFont ?? UIFont.systemFont(ofSize: 17.0)
        self.textField.textColor = (row as? DSYTextPresenterRowDelegate)?.cellFieldTextColor ?? UIColor.black
        self.textField.font = (row as? DSYTextPresenterRowDelegate)?.cellFieldTextFont ?? UIFont.systemFont(ofSize: 15.0)
        self.icon.image = (row as? DSYTextPresenterRowDelegate)?.icon ?? UIImage()
        self.separatorView.backgroundColor = (row as? DSYTextPresenterRowDelegate)?.cellSeparatorViewColor ?? UIColor.gray
        self.backgroundColor = (row as? DSYTextPresenterRowDelegate)?.cellBackgroundColor ?? UIColor.white
        self.headerLabel.text = (row as? DSYTextPresenterRowDelegate)?.cellHeaderText ?? "Titulo"
        row.displayValueFor = nil
        
    }
    
    @objc public func changeRowValue(){
        row.value = self.textField.text
        row.updateCell()
        row.displayValueFor = nil
        if(self.delegate != nil){
            self.delegate?.didTouchButton()
        }
    }

}
