//
//  DSYPickerCell.swift
//  DSYCustomEurekaRow
//
//  Created by Alfredo Luco on 07-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import UIKit

//Picker Cell
public class DSYPickerCell: DSYCell {
    //Setup
    public override func setup() {
        super.setup()
        self.textField.text = (row as? DSYTextPresenterRowDelegate)?.cellFieldText ?? ""
        let pickerView = DSYPickerView(frame: CGRect(x: 0, y: 0, width: super.contentView.frame.width, height: 200))
        pickerView.textField = super.textField
        pickerView.data = (row as? DSYPickerRowDelegate)?.data ?? []
        pickerView.setup()
        row.displayValueFor = nil
        selectionStyle = .none
        row.displayValueFor = nil
        super.textField.tintColor = UIColor.clear
    }
}
