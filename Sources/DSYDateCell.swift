//
//  DSYDateCell.swift
//  DSYCustomEurekaRow
//
//  Created by Alfredo Luco on 07-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import UIKit
import Eureka

//Date Cell
public class DSYDateCell: DSYCell {
    
    //Setup With Picker
    override public func setup() {
        super.setup()
        selectionStyle = .none
        super.textField.text = (row as? DSYTextPresenterRowDelegate)?.cellFieldText ?? ""
        row.displayValueFor = nil
        let datePicker = DSYDatePicker(frame: CGRect(x: 0, y: 0, width: super.contentView.frame.width, height: 200))
        datePicker.textField = super.textField
        datePicker.setup()
        super.textField.inputAccessoryView = datePicker.toolbar
        super.textField.inputView = datePicker
        super.textField.tintColor = UIColor.clear
    }
    
}
