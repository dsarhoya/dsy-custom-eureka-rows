//
//  DSYTextPresenterCell.swift
//  DSYCustomEurekaRow
//
//  Created by Alfredo Luco on 04-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import UIKit
import Eureka

//delegate
public protocol DSYTextPresenterCellDelegate: class {
    func didUpdateField(field: String?, value: String?)
}

//Text Presenter Cell
open class DSYTextPresenterCell: Cell<String>,CellType {

    //header
    @IBOutlet weak var headerLabel: UILabel!
    
    //Field
    @IBOutlet weak open var fieldLabel: UILabel!
    
    //icon Image
    @IBOutlet weak var iconImageView: UIImageView!
    
    //arrow pic
    @IBOutlet weak var arrowImageView: UIImageView!
    
    //Separator View
    @IBOutlet weak var separatorView: UIView!
    
    //delegate
    weak public var delegate: DSYTextPresenterCellDelegate? = nil
    
    //Init
    public required init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        let headerLabel = UILabel()
        let fieldLabel = UILabel()
        let arrowImageView = UIImageView()
        let iconImageView = UIImageView()
        let separatorView = UIView()
        
        self.headerLabel = headerLabel
        self.fieldLabel = fieldLabel
        self.iconImageView = iconImageView
        self.iconImageView.contentMode = .scaleAspectFit
        self.arrowImageView = arrowImageView
        self.arrowImageView.clipsToBounds = true
        self.separatorView = separatorView
        
        self.headerLabel.translatesAutoresizingMaskIntoConstraints = false
        self.fieldLabel.translatesAutoresizingMaskIntoConstraints = false
        self.arrowImageView.translatesAutoresizingMaskIntoConstraints = false
        self.iconImageView.translatesAutoresizingMaskIntoConstraints = false
        self.separatorView.translatesAutoresizingMaskIntoConstraints = false
        
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        let bundle = Bundle(for: self.classForCoder)
        let image = UIImage(named: "arrow", in: bundle, compatibleWith: nil)
        self.arrowImageView.image = image
        
        self.contentView.addSubview(self.headerLabel)
        self.contentView.addSubview(self.fieldLabel)
        self.contentView.addSubview(self.iconImageView)
        self.contentView.addSubview(self.arrowImageView)
        self.contentView.addSubview(self.separatorView)
        
        self.implementConstraints()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    //Constraints Implementation
    func implementConstraints(){
        
        let views : [String : Any] = [
            "header" : self.headerLabel,
            "field" : self.fieldLabel,
            "icon" : self.iconImageView,
            "view" : self.separatorView,
            "arrow" : self.arrowImageView
        ]
        
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-(24)-[icon(30)]-(16)-[header]-(>=16)-[arrow]-(32)-|", options: [], metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-(24)-[icon(30)]-(16)-[field]-(>=16)-[arrow]-(32)-|", options: [], metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "|-(24)-[view]-(24)-|", options: [], metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(16)-[icon(30)]-(>=8)-|", options: [], metrics: nil, views: views))
        self.contentView.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-[header]-(2)-[field]-(8)-[view(1)]-(0)-|", options: [], metrics: nil, views: views))
        self.contentView.addConstraint(NSLayoutConstraint(item: self.arrowImageView, attribute: .centerY, relatedBy: .equal, toItem: self.contentView, attribute: .centerY, multiplier: 1.0, constant: 1.0))
        self.contentView.addConstraint(NSLayoutConstraint(item: self.arrowImageView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1.0, constant: 20.0))
        self.contentView.addConstraint(NSLayoutConstraint(item: self.arrowImageView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1.0, constant: 15.0))
        self.contentView.layoutIfNeeded()
        
    }
    
    //Cell Type Delegate
    
    //setup
    override open func setup() {
        super.setup()
        selectionStyle = .none
    }
    
    //update
    override open func update() {
        super.update()
        
        self.headerLabel.text = (row as? DSYTextPresenterRowDelegate)?.cellHeaderText ?? "Titulo"
        self.fieldLabel.text = (row as? DSYTextPresenterRowDelegate)?.cellFieldText ?? "Detalle"
        self.iconImageView.image = (row as? DSYTextPresenterRowDelegate)?.icon ?? UIImage()
        self.headerLabel.textColor = (row as? DSYTextPresenterRowDelegate)?.cellHeaderTextColor ?? UIColor.black
        self.fieldLabel.textColor = (row as? DSYTextPresenterRowDelegate)?.cellFieldTextColor ?? UIColor.black
        self.headerLabel.font = (row as? DSYTextPresenterRowDelegate)?.cellHeaderTextFont ?? UIFont.systemFont(ofSize: 17.0)
        self.fieldLabel.font = (row as? DSYTextPresenterRowDelegate)?.cellFieldTextFont ?? UIFont.systemFont(ofSize: 15.0)
        self.separatorView.backgroundColor = (row as? DSYTextPresenterRowDelegate)?.cellSeparatorViewColor ?? UIColor.gray
        self.backgroundColor = (row as? DSYTextPresenterRowDelegate)?.cellBackgroundColor ?? UIColor.white
        
        
        if let value = row.value{
            guard value != self.fieldLabel.text else { return }
            self.fieldLabel.text = value != "" ? value : self.fieldLabel.text
            if(self.delegate != nil){
                self.delegate?.didUpdateField(field: row.tag, value: value)
            }
        }
        row.displayValueFor = nil
    }
    
    
}
