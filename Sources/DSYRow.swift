//
//  DSYRow.swift
//  DSYCustomEurekaRow
//
//  Created by Alfredo Luco on 13-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import Foundation
import Eureka

//DSY Button Delegate
public protocol DSYButtonRowDelegate {
    
    //Button Background Color
    var cellButtonColor: UIColor? {get set}
    
    //Button Tint Color
    var cellButtonTintColor: UIColor? {get set}
    
    //Button Text (Title)
    var cellButtonText: String? {get set}
    
    //Button Font
    var cellButtonFont: UIFont? {get set}
}

//Picker Delegate
public protocol DSYPickerRowDelegate {
    
    //Data provided
    var data: [String]? {get set}
    
}

//DSY Row is a generic row for every cell type
open class DSYRow<T: CellType>: Row<T>,DSYTextPresenterRowDelegate,DSYButtonRowDelegate where T : BaseCell{
    
    //Cell Field Text (textfield's text)
    public var cellFieldText: String?
    
    //Cell Header Text
    public var cellHeaderText: String?
    
    //Cell text field font
    public var cellFieldTextFont: UIFont?
    
    //Cell Header Font
    public var cellHeaderTextFont: UIFont?
    
    //Header Color
    public var cellHeaderTextColor: UIColor?
    
    //Text Field Color
    public var cellFieldTextColor: UIColor?
    
    //Cell Background Color
    public var cellBackgroundColor: UIColor?
    
    //Separator View Color
    public var cellSeparatorViewColor: UIColor?
    
    //Placeholder
    public var textFieldPlaceholder: String?
    
    //Icon Image
    public var icon: UIImage?
    
    //Button Font
    public var cellButtonFont: UIFont?
    
    //Button Text
    public var cellButtonText: String?
    
    //Button Color
    public var cellButtonColor: UIColor?
    
    //Button Tint Color
    public var cellButtonTintColor: UIColor?
    
}

//Text Row
public final class DSYTextRow: DSYRow<DSYTextCell>,RowType{
    public required init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<DSYTextCell>()
    }
}


//DSY Date Row
public final class DSYDateRow: DSYRow<DSYDateCell>,RowType{
    public required init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<DSYDateCell>()
    }
}

//Button Row
public final class DSYButtonRow: DSYRow<DSYButtonCell>,RowType{
    public required init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<DSYButtonCell>()
    }
}

//Picker Row
public final class DSYPickerRow: DSYRow<DSYPickerCell>,RowType,DSYPickerRowDelegate{
    
    public var data: [String]?
    
    public required init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<DSYPickerCell>()
    }
}

//Email Row
public final class DSYEmailRow: DSYRow<DSYEmailCell>, RowType{
    public required init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<DSYEmailCell>()
    }
    
}

//Phone Row
public final class DSYPhoneRow: DSYRow<DSYPhoneCell>,RowType{
    public required init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<DSYPhoneCell>()
        displayValueFor = nil
    }
    
}
