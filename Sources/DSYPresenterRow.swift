//
//  DSYPresenterRow.swift
//  DSYCustomEurekaRow
//
//  Created by Alfredo Luco on 13-09-17.
//  Copyright © 2017 dsarhoya. All rights reserved.
//

import Foundation
import Eureka

//Text Presenter Row Delegate
public protocol DSYTextPresenterRowDelegate {
    
    var cellHeaderTextColor: UIColor? {get set}
    
    var cellFieldTextColor: UIColor? {get set}
    
    var cellHeaderText: String? {get set}
    
    var cellFieldText: String? {get set}
    
    var cellHeaderTextFont: UIFont? {get set}
    
    var cellFieldTextFont: UIFont? {get set}
    
    var cellSeparatorViewColor: UIColor? {get set}
    
    var cellBackgroundColor: UIColor? {get set}
    
    var textFieldPlaceholder: String? {get set}
    
    var icon: UIImage? {get set}
}

//Button Delegate
public protocol DSYSendButtonRowDelegate {
    
    var sendButtonColor: UIColor? {get set}
    
    var sendButtonText: String? {get set}
    
    var sendButtonTintColor: UIColor? {get set}
    
    var sendButtonFont: UIFont? {get set}
    
    var sendButtonBackgroundColor: UIColor? {get set}
}

open class DSYPresenterRow<T: CellType, U: TypedRowControllerType>: DSYBasePresenterRow<T, U>,DSYTextPresenterRowDelegate,DSYSendButtonRowDelegate where T : BaseCell, U : UIViewController, T.Value == U.RowValue{
    
    //Header Color
    open var cellHeaderTextColor: UIColor?
    
    //Field Color
    open var cellFieldTextColor: UIColor?
    
    //Header Text
    open var cellHeaderText: String?
    
    //Field Text
    open var cellFieldText: String?
    
    //Header Font
    open var cellHeaderTextFont: UIFont?
    
    //Field Font
    open var cellFieldTextFont: UIFont?
    
    //Separator Color
    open var cellSeparatorViewColor: UIColor?
    
    //Cell Background Color
    open var cellBackgroundColor: UIColor?
    
    //Button Tint Color
    open var sendButtonTintColor: UIColor?
    
    //Button Color
    open var sendButtonColor: UIColor?
    
    //Button Title
    open var sendButtonText: String?
    
    //Button Font Title
    open var sendButtonFont: UIFont?
    
    //Button cell Background Color
    open var sendButtonBackgroundColor: UIColor?
    
    //Cell Icon
    open var icon: UIImage?
    
    //Text Field Text (placeholder)
    open var textFieldPlaceholder: String?

    
}

//Text Presenter Row
public final class DSYTextPresenterRow: DSYPresenterRow<DSYTextPresenterCell, DSYTextViewController>,RowType{
    
    public required init(tag: String?) {
        super.init(tag: tag)
        cellProvider = CellProvider<DSYTextPresenterCell>()
        presentationMode = .show(controllerProvider: ControllerProvider.callback {
            let vc = DSYTextViewController()
            vc.sendButton = self
            vc.cell = self
            return vc // mine is another FormViewController, so no NIB or Storyboard
            }, onDismiss: { vc in
                _ = vc.navigationController?.popViewController(animated: true)
        })
    }
}

//Email Presenter Row
public final class DSYEmailPresenterRow: DSYPresenterRow<DSYTextPresenterCell,DSYEmailViewController>,RowType{
    required public init(tag: String?) {
        super.init(tag: tag)

        cellProvider = CellProvider<DSYTextPresenterCell>()
        presentationMode = .show(controllerProvider: ControllerProvider.callback {
            let vc = DSYEmailViewController()
            vc.sendButton = self
            vc.cell = self
            return vc // mine is another FormViewController, so no NIB or Storyboard
            }, onDismiss: { vc in
                _ = vc.navigationController?.popViewController(animated: true)
        })

    }

}

//Date Presenter Row
public final class DSYDatePresenterRow: DSYPresenterRow<DSYTextPresenterCell,DSYDateViewController>,RowType{
    required public init(tag: String?) {
        super.init(tag: tag)

        cellProvider = CellProvider<DSYTextPresenterCell>()
        presentationMode = .show(controllerProvider: ControllerProvider.callback {
            let vc = DSYDateViewController()
            vc.sendButton = self
            vc.cell = self
            return vc // mine is another FormViewController, so no NIB or Storyboard
            }, onDismiss: { vc in
                _ = vc.navigationController?.popViewController(animated: true)
        })

    }

}

//Phone Presenter Row
public final class DSYPhonePresenterRow: DSYPresenterRow<DSYTextPresenterCell,DSYPhoneViewController>,RowType{
    required public init(tag: String?) {
        super.init(tag: tag)

        cellProvider = CellProvider<DSYTextPresenterCell>()
        presentationMode = .show(controllerProvider: ControllerProvider.callback {
            let vc = DSYPhoneViewController()
            vc.sendButton = self
            vc.cell = self
            return vc // mine is another FormViewController, so no NIB or Storyboard
            }, onDismiss: { vc in
                _ = vc.navigationController?.popViewController(animated: true)
        })

    }

}

//Picker Presenter Row
public final class DSYPickerPresenterRow: DSYPresenterRow<DSYTextPresenterCell,DSYPickerViewController>,RowType,DSYPickerRowDelegate{

    open var data: [String]?

    required public init(tag: String?) {
        super.init(tag: tag)

        cellProvider = CellProvider<DSYTextPresenterCell>()
        presentationMode = .show(controllerProvider: ControllerProvider.callback {
            let vc = DSYPickerViewController()
            vc.sendButton = self
            vc.picker = self
            vc.cell = self
            //            vc.field = self.cell.fieldLabel.text
            return vc // mine is another FormViewController, so no NIB or Storyboard
            }, onDismiss: { vc in
                _ = vc.navigationController?.popViewController(animated: true)
        })

    }

}
